fname = input('Enter the file name: ')

try:
  fhand = open(fname)
except:
  if fname == "are you an elf?":
    print("I am not a gnelf im a gnome - and you've been GNOME'd!")
    exit()
  else:
    print('File cannot be opened:', fname)
    exit()
count = 0
for line in fhand:
    if line.startswith('Subject:') :
        count = count + 1
print('There were', count, 'subject lines in', fname)